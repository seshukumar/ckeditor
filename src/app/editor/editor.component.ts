import { Component, ViewChild } from '@angular/core';
import { CKEditor4 } from 'ckeditor4-angular';

declare const CKEDITOR: any;


@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent {

  @ViewChild('contactList', { static: false }) input;
  @ViewChild('editor', { static: true }) editor;
  editorValue: string;



  config = {
    extraPlugins: 'easyimage',
    removePlugins: 'image',
    removeDialogTabs: 'link:advanced',
    toolbar: [{
      name: 'insert',
      items: ['EasyImageUpload']
    },
    {
      name: 'document',
      items: ['Undo', 'Redo']
    },
    {
      name: 'styles',
      items: ['Format']
    },
    {
      name: 'basicstyles',
      items: ['Bold', 'Italic', 'Strike', '-', 'RemoveFormat']
    },
    {
      name: 'paragraph',
      items: ['NumberedList', 'BulletedList']
    },
    {
      name: 'links',
      items: ['Link', 'Unlink']
    },
    ]
  };



  CONTACTS = [{
    name: 'Huckleberry Finn',
    tel: '+48 1345 234 235',
    email: 'h.finn@example.com',
    avatar: 'hfin'
  },
  {
    name: 'D\'Artagnan',
    tel: '+45 2345 234 235',
    email: 'dartagnan@example.com',
    avatar: 'dartagnan'
  },
  {
    name: 'Phileas Fogg',
    tel: '+44 3345 234 235',
    email: 'p.fogg@example.com',
    avatar: 'pfog'
  },
  {
    name: 'Alice',
    tel: '+20 4345 234 235',
    email: 'alice@example.com',
    avatar: 'alice'
  },
  {
    name: 'Little Red Riding Hood',
    tel: '+45 2345 234 235',
    email: 'lrrh@example.com',
    avatar: 'lrrh'
  },
  {
    name: 'Winnetou',
    tel: '+44 3345 234 235',
    email: 'winnetou@example.com',
    avatar: 'winetou'
  },
  {
    name: 'Edmond Dantès',
    tel: '+20 4345 234 235',
    email: 'count@example.com',
    avatar: 'edantes'
  },
  {
    name: 'Robinson Crusoe',
    tel: '+45 2345 234 235',
    email: 'r.crusoe@example.com',
    avatar: 'rcrusoe'
  }
  ];


  onEditorReady(event) {
    this.init();
  }

  init() {

    CKEDITOR.disableAutoInline = true;

    this.addItems(
      CKEDITOR.document.getById('contactList'),
      new CKEDITOR.template(
        '<div class="contact" data-contact="{id}">' +
        '<p style="color: red">{name}</p>' +
        '</div>'
      ),
      this.CONTACTS
    );
  }


  addItems(listElement, template, items) {

    for (let i = 0, draggable, item; i < items.length; i++) {
      item = new CKEDITOR.dom.element('li');
      draggable = CKEDITOR.dom.element.createFromHtml(
        template.output({
          id: i,
          name: items[i].name
        })
      );

      draggable.setAttributes({
        draggable: 'true',
        tabindex: '0'
      });

      item.append(draggable);
      listElement.append(item);
    }


    CKEDITOR.document.getById('contactList').on('dragstart', (evt) => {
      // The target may be some element inside the draggable div (e.g. the image), so get the div.h-card.
      let target = evt.data.getTarget().getAscendant('div', true);

      // Initialization of the CKEditor 4 data transfer facade is a necessary step to extend and unify native
      // browser capabilities. For instance, Internet Explorer does not support any other data type than 'text' and 'URL'.
      // Note: evt is an instance of CKEDITOR.dom.event, not a native event.
      CKEDITOR.plugins.clipboard.initDragDataTransfer(evt);

      let dataTransfer = evt.data.dataTransfer;

      // You need to set some normal data types to backup values for two reasons:
      // * In some browsers this is necessary to enable drag and drop into text in the editor.
      // * The content may be dropped in another place than the editor.
      dataTransfer.setData('text/html', target.getText());

    });

  }

  onFileUploadRequest(event: CKEditor4.EventInfo): void {
    console.log(`File upload requested ${event}`);
  }

  onFileUploadResponse(event: CKEditor4.EventInfo): void {
    console.log(`File upload responded in ${event}`);
  }

  onDrop($event) {
    console.log("drop event", $event);
  }




}
